import numpy as np
import math
from scipy.integrate import odeint, quad, simps
import matplotlib.pyplot as plt
plt.rcParams.update({'figure.autolayout': True})
plt.rcParams.update({'font.size': 14})