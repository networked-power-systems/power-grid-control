"""
Author: Randall Martyr
Date: 3rd August 2018
This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License
(https://creativecommons.org/licenses/by-sa/4.0/).
"""

from common_imports import *
import frequency_methods, frequency_methods_enhanced, mechanical_power_methods, control_methods


def solutionPlot(time_args, state_args, controls, oc_args, nominal_frequency=50.0,
                 legend=True, save_plot=False, save_folder='./', prefix=''):

    num_partitions = time_args["number of partitions"]
    var_knots = time_args["variable knots"]
    fixed_knots = time_args["fixed knots"]
    num_nodes = state_args["number of nodes"]
    mechanical_power_args = state_args["mechanical power"]
    ec_variables = time_args["decision variables"]
    terminal_time = time_args["terminal time"]
    enhancing_control_args = (ec_variables, fixed_knots, num_partitions)
    voltage_constraints = oc_args["constraints"]["voltage"]

    frequency_deviation_constraints = oc_args["constraints"]["frequency deviation"]
    
    for control in controls:

        integral_controller = "is integral" in control.keys() and control["is integral"]

        if integral_controller:
            # need to reset integral controller
            control["args"]["args"]["old angular velocity"] = np.zeros(num_nodes, dtype=np.float64)
            control["args"]["args"]["old total"] = np.zeros(num_nodes, dtype=np.float64)
            control["args"]["args"]["old time"] = 0.0

        state_ts = frequency_methods_enhanced.integrate_state(fixed_knots, time_args, state_args, control["args"],
                                                              enhancing_control_args, feedback=control["feedback"])

        if integral_controller:
            # need to reset integral controller
            control["args"]["args"]["old angular velocity"] = np.zeros(num_nodes, dtype=np.float64)
            control["args"]["args"]["old total"] = np.zeros(num_nodes, dtype=np.float64)
            control["args"]["args"]["old time"] = 0.0

        if control["feedback"]:
            control_ts = np.array([frequency_methods.feedback_control(var_knots[index], state_ts[index, :],
                                                                      control["args"]) for index
                                    in xrange(num_partitions)])

            if integral_controller:
                # need to reset integral controller
                control["args"]["args"]["old angular velocity"] = np.zeros(num_nodes, dtype=np.float64)
                control["args"]["args"]["old total"] = np.zeros(num_nodes, dtype=np.float64)
                control["args"]["args"]["old time"] = 0.0

            open_loop_control_args = {
                "func": control_methods.piecewise_constant,
                "args": {"parameters": control_ts, "number of partitions": num_partitions,
                         "number of nodes": num_nodes, "terminal time": terminal_time}
            }
            # re-run system dynamics with open loop control
            state_ts = frequency_methods_enhanced.integrate_state(fixed_knots, time_args, state_args,
                                                                  open_loop_control_args,
                                                                  enhancing_control_args, feedback=True
                                                                  )
        else:
            control_ts = np.array([frequency_methods.control(var_knots[index], *control["args"]) for index
                                    in xrange(num_partitions)])

        frequency_deviation_ts = state_ts[:, num_nodes * 1:num_nodes * 2]

        frequency_dispersion_ts = np.std(frequency_deviation_ts, axis=1)
        mean_frequency_deviation_ts = np.mean(frequency_deviation_ts, axis=1)
        voltage_ts = state_ts[:, num_nodes * 2:num_nodes * 3]
        mean_voltage_ts = np.mean(voltage_ts - voltage_ts[0, :], axis=1)
        # std_voltage_ts = np.std(voltage_ts - voltage_ts[0, :], axis=1)
        mechanical_power_ts = np.array([mechanical_power_methods.mechanical_power(t, mechanical_power_args) for t in
                                        var_knots])

        control["results"] = {
            "frequency deviation": frequency_deviation_ts,
            "frequency dispersion": frequency_dispersion_ts,
            "mean frequency deviation": mean_frequency_deviation_ts,
            "voltage": voltage_ts,
            "control": control_ts,
            "mean voltage": mean_voltage_ts,
            # "voltage std": std_voltage_ts,
            "power injection": mechanical_power_ts
        }

    linestyles = ['-', '--', '-.', ':']

    for control in controls:
        f, axarr = plt.subplots(3, sharex=True)#, figsize=(15,15))

        axarr[0].set_title(control["label"]+': Angular velocity $[rad/s]$')
        axarr[0].set_ylabel(r'$\mathbf{\omega}$', fontsize=20)
        # axarr[0].axhline(linewidth=1, color='k', linestyle='--', y=frequency_deviation_constraints[0])
        # axarr[0].axhline(linewidth=1, color='k', linestyle='--', y=frequency_deviation_constraints[1])
        axarr[1].set_title(control["label"]+': Voltage [pu]')
        axarr[1].set_ylabel(r'$\mathbf{V}$', fontsize=20)
        axarr[2].set_title(control["label"]+': Controlled active power [pu]')
        axarr[2].set_ylabel('$\mathbf{u}$', fontsize=20)

        lns = axarr[1].plot()

        for i in xrange(num_nodes):
            lineno = i % len(linestyles)
            axarr[0].plot(var_knots, control["results"]["frequency deviation"][:, i], label=r'$\omega_{' + str(i + 1) + '}$',
                          linewidth=2.0, linestyle=linestyles[lineno])
            lns += axarr[1].plot(var_knots, control["results"]["voltage"][:, i], label=r'node ${' + str(i + 1) + '}$', linewidth=2.0,
                          linestyle=linestyles[lineno])
            axarr[2].plot(var_knots[:num_partitions], control["results"]["control"][:, i], label=r'$u_{' + str(i + 1) + '}$',
                          linewidth=2.0, linestyle=linestyles[lineno])

        axarr[0].set_yticks(
            np.linspace(np.min(control["results"]["frequency deviation"]), np.max(control["results"]["frequency deviation"]), 5))
        ax1 = axarr[0].twinx()
        ax1_yticks = nominal_frequency + axarr[0].get_yticks() / (2.0 * math.pi)
        ax1.set_yticks(ax1_yticks)
        ax1.set_yticklabels(ax1_yticks)
        ax1_ylim = nominal_frequency + (np.asarray(axarr[0].get_ylim())) / (2.0 * math.pi)
        ax1.set_ylim(ax1_ylim)
        ax1.set_ylabel("$Hz$")

        axarr[1].set_yticks(
            np.linspace(np.min(control["results"]["voltage"]), np.max(control["results"]["voltage"]), 5))

        axarr[2].set_yticks(
            np.linspace(np.min(control["results"]["control"]), np.max(control["results"]["control"]), 5))

        from matplotlib.ticker import FormatStrFormatter
        axarr[0].yaxis.set_major_formatter(FormatStrFormatter('%.2f'))
        ax1.yaxis.set_major_formatter(FormatStrFormatter('%.3f'))
        axarr[1].yaxis.set_major_formatter(FormatStrFormatter('%.3f'))
        axarr[2].yaxis.set_major_formatter(FormatStrFormatter('%.2f'))

        box = axarr[1].get_position()
        axarr[1].set_position([box.x0, box.y0, box.width, box.height])

        labs = [l.get_label() for l in lns]
        # axarr[1].legend(lns, labs, loc='center left', bbox_to_anchor=(1.075, 1.15))

        plt.xlabel('Time [s]', fontsize=20)
        # plt.xticks(np.arange(0, terminal_time+0.5, 10.0))
        plt.xticks(np.linspace(0, terminal_time, 7))
        plt.xlim([0, terminal_time])
        plt.tight_layout()
        if save_plot:
            plt.savefig(save_folder + prefix + "-" + control["label"] + "-frequency_voltage_and_control.eps", format='eps', dpi=300, bbox_inches='tight')
        else:
            plt.show()

        # 2 subplots, the axes array is 1-d
        f, axarr = plt.subplots(2, sharex=True)
        lns = axarr[1].plot()
        axarr[0].set_title(control["label"]+': Mean angular velocity $[rad/s]$')
        axarr[0].axhline(linewidth=1, color='r', linestyle=':', y=frequency_deviation_constraints[0])
        axarr[0].axhline(linewidth=1, color='r', linestyle=':', y=frequency_deviation_constraints[1])
        y_ticks = np.linspace(frequency_deviation_constraints[0], frequency_deviation_constraints[1], 5)
        axarr[0].set_yticks(y_ticks)
        axarr[0].set_ylabel(r"$\langle\omega\rangle$", fontsize=20)
        # Create secondary axis to show equivalent values in Hz
        ax1 = axarr[0].twinx()
        ax1_yticks = nominal_frequency + (y_ticks / (2.0 * math.pi))
        ax1.set_yticks(ax1_yticks)
        ax1.set_yticklabels(ax1_yticks)
        ax1_ylim = nominal_frequency + (np.asarray(axarr[0].get_ylim())) / (2.0 * math.pi)
        ax1.set_ylim(ax1_ylim)
        ax1.set_ylabel("$Hz$")
        axarr[1].set_title(control["label"]+': Dispersion of angular velocity $[rad/s]$')
        axarr[1].set_ylabel(r'$\sigma(\mathbf{\omega})$', fontsize=20)
        axarr[0].plot(var_knots, control["results"]["mean frequency deviation"],
              label=control["label"], color='k', linewidth=2.0)
        lns += axarr[1].plot(var_knots, control["results"]["frequency dispersion"],
                             label=control["label"], color='k', linewidth=2.0)

        plt.xlabel('Time [s]', fontsize=20)
        plt.xticks(np.linspace(0, terminal_time, 7))
        plt.xlim([0, terminal_time])
        plt.tight_layout()
        if save_plot:
            plt.savefig(save_folder + prefix + "-" + control["label"] + "-system_stability.eps", format='eps', dpi=300,
                        bbox_inches='tight')
        else:
            plt.show()


def plotDisturbances(disturbances, var_knots, initial_time, terminal_time, title='', save_plot=False, save_folder='./', prefix=''):
    plt.figure()
    ax = plt.subplot(111)
    count = 0
    linestyles = ['-.', '-']
    min_disturbance = np.inf
    for disturbance in disturbances:
        disturbance_ts = np.array([disturbance["func"](t, disturbance["args"])[0] for t in var_knots])
        min_disturbance = min(min_disturbance, min(disturbance_ts))
        ax.plot(var_knots, disturbance_ts, label=disturbance["label"], linestyle=linestyles[count],
                linewidth=2.0, color='k')
        count += 1
    ax.set_ylabel(r'$\xi_{1}\; [pu]$', fontsize=20)
    plt.xlabel('Time [s]', fontsize=20)
    plt.xlim([initial_time, terminal_time])
    plt.ylim(ymin=min_disturbance-0.1, ymax=0.1)
    plt.xticks(np.linspace(0, terminal_time, 7))
    box = ax.get_position()
    ax.set_position([box.x0, box.y0, box.width * 0.95, box.height])
    ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
    plt.tight_layout()
    if save_plot:
        plt.savefig(save_folder + prefix + "disturbances.eps", format='eps', dpi=300, bbox_inches='tight')
        plt.close()
    else:
        plt.show()


def plotCosts(costs, columns, labels, colors, title='', save_plot=False, save_folder='./', prefix=''):
    import pandas as pd
    df = pd.DataFrame(costs, columns=columns)
    ax = df.plot.bar(color=colors)
    ax.set_ylabel("$J(\mathbf{u})$")
    ax.set_xticklabels(labels, rotation=0)
    if save_plot:
        plt.savefig(save_folder + prefix + "costs.eps", format='eps', dpi=300, bbox_inches='tight')
        plt.close()
    else:
        plt.show()
