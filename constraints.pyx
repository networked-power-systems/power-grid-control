from common_imports import *
import frequency_methods_enhanced as frequency_methods
cimport cython
from libc.math cimport exp


# constraints in canonical form: Teo et al p. 131
# I only have continuous inequality constraints, but reminder to use a better approximation for the inequality
# constraints that do not depend explicitly on the control variable: frequency_dispersion, mean_frequency_deviation,
# voltage

"""
General purpose functions

"""


def heaviside(double x):
    if x > 0.0:
        return 1.0
    elif x == 0.0:
        return 0.5
    else:
        return 0.0


def standard_logistic(double y):
    cdef double value = 1.0 / (1 + exp(-y))
    return value


def standard_logistic_derivative(double y):
    cdef double logistic_val = standard_logistic(y)
    cdef double derivative = logistic_val * (1.0 - logistic_val)
    return derivative


def upper_constraint(double x, double upper_bound):
    cdef double constraint_value = upper_bound - x
    return constraint_value


def box_constraint(double x, double lower_bound, double upper_bound):
    cdef double constraint_value = (upper_bound - x) * (x - lower_bound)
    return constraint_value


def upper_inequality_constraint_derivative(double y_value, y_derivative_value, constraint):
    # apply chain rule to calculate d/dx(min(\psi(f(x)),0))^{2}, where \psi(\cdot) is the constraint function
    cdef double constraint_value = upper_constraint(y_value, *constraint)
    cdef double derivative_squared_term = 2.0 * (min(0, constraint_value))
    derivative_constraint_term = -1.0 * y_derivative_value
    derivative = derivative_squared_term * derivative_constraint_term
    return derivative


def box_inequality_constraint_derivative(double y_value, y_derivative_value, constraints):
    # apply chain rule to calculate d/dx(min(\psi(f(x)),0))^{2}, where \psi(\cdot) is the constraint function
    cdef double constraint_value = box_constraint(y_value, *constraints)
    cdef double derivative_squared_term = 2.0 * (min(0, constraint_value))
    derivative_constraint_term = (np.sum(constraints) - (2.0 * y_value)) * y_derivative_value
    derivative = derivative_squared_term * derivative_constraint_term
    return derivative


def inequality_constraint_helper(double x, constraints, constraint_function):
    cdef double constraint_value = constraint_function(x, *constraints)
    cdef double constraint_wrapper_value = (min(0.0, constraint_value))**2
    return constraint_wrapper_value

########################################################################################################################


def terminal_time_cost(double integrated_terminal_time, double actual_terminal_time):
    cdef double cost = (integrated_terminal_time - actual_terminal_time)
    return cost


def terminal_time_performance(enhancing_control_args, *constraint_args):
    cdef double integrated_terminal_time = frequency_methods.exact_time_transform(1.0, *enhancing_control_args)
    cdef double cost = terminal_time_cost(integrated_terminal_time, *constraint_args)
    return cost

########################################################################################################################