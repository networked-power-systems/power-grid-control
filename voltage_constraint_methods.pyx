from common_imports import *
import frequency_methods_enhanced as frequency_methods_e
import frequency_methods
import constraints

########################################################################################################################
"""
Evaluation of constraint costs
"""
########################################################################################################################


def voltage_lagrange(state_values, int num_nodes, int this_node, voltage_constraint):
    cdef double voltage_value = state_values[(2*num_nodes) + this_node]
    cdef double lagrange_cost = constraints.inequality_constraint_helper(voltage_value, voltage_constraint, constraints.box_constraint)
    return lagrange_cost


def voltage_cost_from_ts(double s, int this_node, state_ts, enhancing_control_args, int num_partitions, int num_nodes,
                         voltage_constraint):
    state_values = state_ts[min(int(s * num_partitions), num_partitions)]
    cdef double f_value_time = frequency_methods_e.enhancing_control_equation_of_motion(s, *enhancing_control_args)
    cdef double cost = f_value_time * voltage_lagrange(state_values, num_nodes, this_node, voltage_constraint)
    return cost


def voltage_performance(int this_node, state_ts, fixed_knots, enhancing_control_args, int num_partitions, int num_nodes,
                        voltage_constraint, double mayer_weight):
    cdef double s
    cdef double performance_value = simps([voltage_cost_from_ts(s, this_node, state_ts, enhancing_control_args, num_partitions, num_nodes,
                                   voltage_constraint) for s in fixed_knots], x=fixed_knots)
    performance_value += (mayer_weight * voltage_lagrange(state_ts[-1], num_nodes, this_node, voltage_constraint))
    return performance_value


def voltage_constraint_gradient(int node, state_values, int num_nodes, voltage_constraint):
    # voltage constraint at each node
    voltage_gradient = np.zeros_like(state_values)
    voltage_gradient[2*num_nodes + node] = 1.0
    cdef double voltage_value = state_values[2*num_nodes + node]
    constraint_gradient = constraints.box_inequality_constraint_derivative(voltage_value, voltage_gradient, voltage_constraint)
    return constraint_gradient


########################################################################################################################
"""
Costate variables: dp/ds = -^H_{(t,x)}(s,(t(s),x(t(s))),u(t(s)),p(s)); p(1) = M_{(x,t)}(x(t(1)),t(1)).
"""
########################################################################################################################


def voltage_costate_eom(costate, double s, int node, state_ts, int num_partitions, int num_nodes, state_args,
                        enhancing_control_args, voltage_constraint):
    return -voltage_hamiltonian_state_gradient(costate, s, node, state_ts, num_partitions, num_nodes, state_args,
                                               enhancing_control_args, voltage_constraint)


def voltage_costate_eom_backward(costate, double s, int node, state_ts, int num_partitions, int num_nodes, state_args,
                                 enhancing_control_args, voltage_constraint):
    return -voltage_costate_eom(costate, -s, node, state_ts, num_partitions, num_nodes, state_args,
                                enhancing_control_args, voltage_constraint)


def integrate_voltage_costate(int node, state_ts, fixed_knots, int num_partitions, int num_nodes, state_args,
                              enhancing_control_args, voltage_constraint, double mayer_weight):
    costate_terminal_value = mayer_weight * voltage_constraint_gradient(node, state_ts[-1], num_nodes, voltage_constraint)
    costate_ts = odeint(voltage_costate_eom_backward, costate_terminal_value, -fixed_knots[::-1],
                  args=(node, state_ts, num_partitions, num_nodes, state_args, enhancing_control_args,
                        voltage_constraint),
                        tcrit=(-fixed_knots[::-1])[1:-1]
                  )[::-1]
    return costate_ts

########################################################################################################################
"""
Gradient of Hamiltonian w.r.t to space-time state variable (x,t)
"""
########################################################################################################################


def voltage_hamiltonian_state_gradient(costate, double s, int node, state_ts, int num_partitions, int num_nodes, state_args,
                                       enhancing_control_args, voltage_constraint):
    state_values = state_ts[min(int(s * num_partitions), num_partitions)]
    cdef double f_value_time = frequency_methods_e.enhancing_control_equation_of_motion(s, *enhancing_control_args)
    jacobian = frequency_methods.equations_of_motion_spatial_jacobian(state_values, state_args)
    gradient_value = np.dot(costate.T, jacobian)
    gradient_value += voltage_constraint_gradient(node, state_values, num_nodes, voltage_constraint)
    gradient_value *= f_value_time
    return gradient_value


########################################################################################################################
"""
Derivative of Hamiltonian w.r.t to a component of the control variable
"""
########################################################################################################################


def voltage_hamiltonian_control_derivative(double s, state_ts, costate_ts, int num_partitions, int node, state_args,
                                           enhancing_control_args):
    costate = costate_ts[min(int(s * num_partitions), num_partitions)]
    state_values = state_ts[min(int(s * num_partitions), num_partitions)]
    cdef double f_value_time = frequency_methods_e.enhancing_control_equation_of_motion(s, *enhancing_control_args)
    # the lagrange term is independent of the control since this is a pure state constraint
    eom_control_derivative = frequency_methods.equations_of_motion_control_derivative(node, state_args)
    cdef double derivative_value = np.dot(costate.T, eom_control_derivative) * f_value_time
    return derivative_value


def voltage_constraint_control_parameter_gradient(state_ts, costate_ts, fixed_knots, int num_partitions, int num_nodes,
                                                  state_args, enhancing_control_args):
    # there are N = num_nodes control variables, each of which is parametrised by n_{p} = num_partitions control values
    # [u^{1},...,u^{N}], u^{i} = [u^{i}_{1},...,u^{i}_{n_{p}}]
    cdef int this_node, this_partition
    gradient = np.array([quad(voltage_hamiltonian_control_derivative,
                               fixed_knots[this_partition], fixed_knots[this_partition + 1],
                               args=(state_ts, costate_ts, num_partitions, this_node, state_args, enhancing_control_args))[0]
                         for this_node in xrange(num_nodes) for this_partition in xrange(num_partitions)])
    return gradient