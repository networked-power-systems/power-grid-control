from common_imports import *
cimport cython
from libc.math cimport exp
from libc.math cimport sin
from libc.math cimport cos

def standard_logistic(double y):
    cdef double value = 1.0 / (1 + exp(-y))
    return value

def standard_logistic_derivative(double y):
    cdef double value = standard_logistic(y) * (1.0 - standard_logistic(y))
    return value


def interval_indicator(double x, double a, double b, double steepness):
    cdef double indicator = standard_logistic(steepness*(x-a)) * standard_logistic(steepness*(b-x))
    return indicator


def interval_indicator_derivative(double x, double a, double b, double steepness):
    cdef double left_hand_side_indicator = standard_logistic(steepness*(x-a))
    cdef double right_hand_side_indicator = standard_logistic(steepness*(b-x))
    cdef double left_hand_side_derivative = left_hand_side_indicator * (1.0 - left_hand_side_indicator) * steepness
    cdef double right_hand_side_derivative = right_hand_side_indicator * (1.0 - right_hand_side_indicator) * (-steepness)
    cdef double derivative = (left_hand_side_derivative * right_hand_side_indicator) + (right_hand_side_derivative * left_hand_side_indicator)
    return derivative


def periodic_disturbances(double t, mechanical_power_args):
    disturbance_params = mechanical_power_args["disturbance parameters"]
    cdef int num_nodes = mechanical_power_args["number of nodes"]
    disturbances = np.zeros(num_nodes)
    cdef double arg = 0.0
    cdef double disturbance_sin_value, disturbance_cos_value, disturbance_frequency
    cdef int node
    for node in disturbance_params:
        for disturbance_sin_value, disturbance_cos_value, disturbance_frequency in disturbance_params[node]:
            arg = 2.0*math.pi*t*disturbance_frequency
            disturbances[node-1] += ((disturbance_sin_value * sin(arg)) + (disturbance_cos_value * cos(arg)))
    return disturbances


def periodic_disturbance_derivatives(double t, mechanical_power_args):
    disturbance_params = mechanical_power_args["disturbance parameters"]
    cdef int num_nodes = mechanical_power_args["number of nodes"]
    disturbances = np.zeros(num_nodes)
    cdef double arg = 0.0
    cdef int node
    cdef double disturbance_sin_value, disturbance_cos_value, disturbance_frequency
    for node in disturbance_params:
        for disturbance_sin_value, disturbance_cos_value, disturbance_frequency in disturbance_params[node]:
            arg = 2.0*math.pi*disturbance_frequency
            disturbances[node-1] += ((disturbance_sin_value * arg * cos(arg*t)) - (disturbance_cos_value * arg * sin(arg*t)))
    return disturbances


def bump_disturbance_derivatives(double t, mechanical_power_args):
    disturbance_params = mechanical_power_args["disturbance parameters"]
    cdef int num_nodes = mechanical_power_args["number of nodes"]
    cdef double logistic_steepness = mechanical_power_args["steepness"]
    derivatives = np.zeros(num_nodes, dtype=np.float64)
    cdef int node
    for node in disturbance_params:
        for disturbance_value, disturbance_start, disturbance_end in disturbance_params[node]:
            derivatives[node-1] += (disturbance_value * interval_indicator_derivative(t, disturbance_start,
                                                                                      disturbance_end,
                                                                                      logistic_steepness))
    return derivatives


# step function disturbances where indicators have been approximated by logistic functions
def bump_disturbances(double t, mechanical_power_args):
    disturbance_params = mechanical_power_args["disturbance parameters"]
    cdef int num_nodes = mechanical_power_args["number of nodes"]
    cdef double logistic_steepness = mechanical_power_args["steepness"]
    disturbances = np.zeros(num_nodes)
    cdef int node
    cdef double disturbance_value, disturbance_start, disturbance_end
    for node in disturbance_params:
        for disturbance_value, disturbance_start, disturbance_end in disturbance_params[node]:
            disturbances[node-1] += (disturbance_value * interval_indicator(t, disturbance_start, disturbance_end, logistic_steepness))
    return disturbances


def heaviside_disturbance_derivatives(double t, mechanical_power_args):
    disturbance_params = mechanical_power_args["disturbance parameters"]
    cdef int num_nodes = mechanical_power_args["number of nodes"]
    cdef double logistic_steepness = mechanical_power_args["steepness"]
    derivatives = np.zeros(num_nodes, dtype=np.float64)
    cdef int node
    cdef double disturbance_value, disturbance_start
    for node in disturbance_params:
        for disturbance_value, disturbance_start in disturbance_params[node]:
            derivatives[node-1] += (disturbance_value * standard_logistic_derivative(logistic_steepness*(t-disturbance_start)) * logistic_steepness)
    return derivatives


# heaviside disturbances where indicators have been approximated by logistic functions
def heaviside_disturbances(double t, mechanical_power_args):
    disturbance_params = mechanical_power_args["disturbance parameters"]
    cdef int num_nodes = mechanical_power_args["number of nodes"]
    cdef double logistic_steepness = mechanical_power_args["steepness"]
    disturbances = np.zeros(num_nodes)
    cdef int node
    cdef double disturbance_value, disturbance_start
    for node in disturbance_params:
        for disturbance_value, disturbance_start in disturbance_params[node]:
            disturbances[node-1] += (disturbance_value * standard_logistic(logistic_steepness*(t-disturbance_start)))
    return disturbances


def time_derivative_mechanical_power(double t, mechanical_power_args):
    derivative_mechanical_power_value = mechanical_power_args["derivative"](t, mechanical_power_args["args"])
    return derivative_mechanical_power_value


def mechanical_power(double t, mechanical_power_args):
    initial_mechanical_power = mechanical_power_args["args"]["initial mechanical power"]
    mechanical_power_value = initial_mechanical_power + mechanical_power_args["func"](t, mechanical_power_args["args"])
    return mechanical_power_value