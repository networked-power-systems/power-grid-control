from common_imports import *

import frequency_methods_enhanced
import performance_methods
import frequency_deviation_constraint_methods
import frequency_dispersion_constraint_methods
import voltage_constraint_methods


"""
The CPET method (Teo et al) is used to obtain a non-linear program, the optimal parameter selection problem,
that approximates the optimal control problem with continuous constraints in canonical form. We use the 
package scipy.optimize.fmin_slsqp to implement the sequential quadratic programming algorithm for this non-linear
program.
"""

########################################################################################################################
"""
Objective and constraint functions
"""
########################################################################################################################


# Objective function
def objective(parameters, time_args, state_args, oc_args):
    cdef int num_partitions = time_args["number of partitions"]
    var_knots = time_args["variable knots"]
    fixed_knots = time_args["fixed knots"]
    cdef int num_nodes = state_args["number of nodes"]
    objective_args = oc_args["objective"]
    control_variables = np.array(np.split(parameters, num_nodes)).T
    ec_variables = time_args["decision variables"]
    cdef double terminal_time = time_args["terminal time"]
    control_args = (control_variables, num_partitions, num_nodes, terminal_time)
    enhancing_control_args = (ec_variables, fixed_knots, num_partitions)
    cdef double value = performance_methods.performance_criterion(fixed_knots, num_partitions, control_args, enhancing_control_args, *objective_args)
    return value

def objective_gradient(parameters, time_args, state_args, oc_args):
    cdef int num_partitions = time_args["number of partitions"]
    var_knots = time_args["variable knots"]
    fixed_knots = time_args["fixed knots"]
    cdef int num_nodes = state_args["number of nodes"]
    objective_args = oc_args["objective"]
    control_variables = np.array(np.split(parameters, num_nodes)).T
    ec_variables = time_args["decision variables"]

    cdef double terminal_time = time_args["terminal time"]
    control_args = (control_variables, num_partitions, num_nodes, terminal_time)
    enhancing_control_args = (ec_variables, fixed_knots, num_partitions)

    state_ts = frequency_methods_enhanced.integrate_state(fixed_knots, time_args, state_args, control_args,
                                                          enhancing_control_args)

    costate_ts = performance_methods.integrate_performance_costate(state_ts, fixed_knots, num_partitions, state_args,
                                                                   enhancing_control_args)

    gradient_value = performance_methods.performance_control_parameter_gradient(costate_ts, fixed_knots, num_partitions,
    num_nodes, state_args, control_args, enhancing_control_args, objective_args)

    return gradient_value


########################################################################################################################
# Constraints
########################################################################################################################

def ieq_constraints(parameters, time_args, state_args, oc_args):
    cdef int num_partitions = time_args["number of partitions"]
    fixed_knots = time_args["fixed knots"]
    var_knots = time_args["variable knots"]
    cdef int num_nodes = state_args["number of nodes"]

    control_variables = np.array(np.split(parameters, num_nodes)).T
    ec_variables = time_args["decision variables"]

    cdef double terminal_time = time_args["terminal time"]
    control_args = (control_variables, num_partitions, num_nodes, terminal_time)
    enhancing_control_args = (ec_variables, fixed_knots, num_partitions)

    state_ts = frequency_methods_enhanced.integrate_state(fixed_knots, time_args, state_args, control_args,
                                                          enhancing_control_args)

    constraint_values = []
    cdef double mayer_weight = oc_args["mayer weights"]["frequency dispersion"]

    cdef double fd_performance = frequency_dispersion_constraint_methods.frequency_dispersion_performance(
        state_ts, fixed_knots, enhancing_control_args, num_partitions, num_nodes, mayer_weight)

    # frequency dispersion constraint
    cdef double fd_ieq_tol = oc_args["tolerance"]["frequency dispersion"]
    cdef double frequency_dispersion_constraint_value = fd_ieq_tol - fd_performance
    constraint_values.append(frequency_dispersion_constraint_value)

    # frequency deviation constraint
    cdef double freq_dev_tol = oc_args["tolerance"]["frequency deviation"]
    frequency_deviation_constraint = oc_args["constraints"]["frequency deviation"]
    mayer_weight = oc_args["mayer weights"]["frequency deviation"]
    cdef double fdev_performance = frequency_deviation_constraint_methods.mean_frequency_deviation_performance(
            state_ts, fixed_knots, enhancing_control_args, num_partitions, num_nodes, frequency_deviation_constraint, mayer_weight)
    cdef double frequency_deviation_constraint_value = freq_dev_tol - fdev_performance

    constraint_values.append(frequency_deviation_constraint_value)

    # voltage constraints

    cdef double voltage_tol = oc_args["tolerance"]["voltage"]
    voltage_constraint = oc_args["constraints"]["voltage"]
    mayer_weight = oc_args["mayer weights"]["voltage"]
    cdef double voltage_constraint_value
    cdef int node
    for node in xrange(num_nodes):
        voltage_performance = voltage_constraint_methods.voltage_performance(node, state_ts, fixed_knots, enhancing_control_args, num_partitions,
                                                                 num_nodes, voltage_constraint[node], mayer_weight)
        voltage_constraint_value = voltage_tol - voltage_performance
        constraint_values.append(voltage_constraint_value)

    return np.array(constraint_values)

def ieq_constraints_grad(parameters, time_args, state_args, oc_args):
    cdef int num_partitions = time_args["number of partitions"]
    var_knots = time_args["variable knots"]
    fixed_knots = time_args["fixed knots"]
    cdef int num_nodes = state_args["number of nodes"]
    control_variables = np.array(np.split(parameters, num_nodes)).T
    ec_variables = time_args["decision variables"]

    cdef double terminal_time = time_args["terminal time"]
    control_args = (control_variables, num_partitions, num_nodes, terminal_time)
    enhancing_control_args = (ec_variables, fixed_knots, num_partitions)

    state_ts = frequency_methods_enhanced.integrate_state(fixed_knots, time_args, state_args, control_args,
                                                          enhancing_control_args)

    gradient_values = []

    # frequency dispersion constraint
    cdef double mayer_weight = oc_args["mayer weights"]["frequency dispersion"]

    f_disp_costate_ts = frequency_dispersion_constraint_methods.integrate_frequency_dispersion_costate(state_ts, fixed_knots, num_partitions,
                                                                            num_nodes, state_args,
                                                                            enhancing_control_args, mayer_weight)

    f_disp_gradient_value = (frequency_dispersion_constraint_methods.frequency_dispersion_constraint_control_parameter_gradient(
            state_ts, f_disp_costate_ts, fixed_knots, num_partitions, num_nodes, state_args, enhancing_control_args) * -1.0)

    gradient_values.append(f_disp_gradient_value)

    # frequency deviation constraint
    constraint_args = oc_args["constraints"]["frequency deviation"]
    mayer_weight = oc_args["mayer weights"]["frequency deviation"]
    f_dev_costate_ts = frequency_deviation_constraint_methods.integrate_frequency_deviation_costate(state_ts, fixed_knots, num_partitions,
                                                                           num_nodes, state_args,
                                                                           enhancing_control_args, constraint_args,
                                                                           mayer_weight)

    f_dev_gradient_value = (frequency_deviation_constraint_methods.frequency_deviation_constraint_control_parameter_gradient(
            state_ts, f_dev_costate_ts, fixed_knots, num_partitions, num_nodes, state_args, enhancing_control_args) * -1.0)

    gradient_values.append(f_dev_gradient_value)

    # voltage constraints
    constraint_args = oc_args["constraints"]["voltage"]
    mayer_weight = oc_args["mayer weights"]["voltage"]
    cdef int node
    for node in xrange(num_nodes):
        voltage_costate_ts = voltage_constraint_methods.integrate_voltage_costate(node, state_ts, fixed_knots, num_partitions, num_nodes,
                                                               state_args, enhancing_control_args, constraint_args[node],
                                                               mayer_weight)

        voltage_gradient_value = (voltage_constraint_methods.voltage_constraint_control_parameter_gradient(
            state_ts, voltage_costate_ts, fixed_knots, num_partitions, num_nodes, state_args, enhancing_control_args) * -1.0)

        gradient_values.append(voltage_gradient_value)

    return np.array(gradient_values)