from common_imports import *
cimport cython
from libc.math cimport exp
from libc.math cimport sin
from libc.math cimport cos
import mechanical_power_methods
import control_methods


def control(double t, control_parameters, int num_partitions, int num_nodes, double terminal_time):
    cdef int knot_index = int(num_partitions * t / terminal_time)
    control_value = control_parameters[knot_index] if knot_index < num_partitions else np.zeros(num_nodes)
    return control_value


def feedback_control(double t, state, control_args):
    return control_methods.control(t, state, control_args)


"""
method for numerical integration of ode y' = f(y,t)
f describes the equations of motion for the vector y
"""


def equations_of_motion(state_values, double t, time_args, state_args, control_args):
    cdef int num_nodes = state_args["number of nodes"]
    mechanical_power_args = state_args["mechanical power"]
    coupling_matrix = state_args["coupling matrix"]
    voltage_reference = state_args["voltage references"]
    transient_time_constant = state_args["machine parameters"]["transient time constant"]
    damping_constant = state_args["machine parameters"]["damping constant"]
    direct_synchronous_reactance_difference = state_args["machine parameters"]["synchronous reactance difference"]
    inertia = state_args["machine parameters"]["inertia"]
    rotor_angle = state_values[:num_nodes]
    frequency_deviation = state_values[num_nodes:2*num_nodes]
    voltage = state_values[2*num_nodes:3*num_nodes]
    f_values = np.zeros(3*num_nodes, dtype=np.float64)
    cdef double rotor_angle_difference
    cdef int i, j
    for i in xrange(num_nodes):
        f_values[i] = frequency_deviation[i]
        f_values[num_nodes+i] = mechanical_power(t, mechanical_power_args)[i] + control(t, *control_args)[i] - (damping_constant[i] * frequency_deviation[i])
        for j in xrange(num_nodes):
            rotor_angle_difference = rotor_angle[i] - rotor_angle[j]
            f_values[num_nodes+i] -= (coupling_matrix[i, j] * voltage[i] * voltage[j] * sin(rotor_angle_difference))
            f_values[(2*num_nodes)+i] += (coupling_matrix[i, j] * voltage[j] * cos(rotor_angle_difference))
        f_values[num_nodes+i] /= inertia[i]
        f_values[(2*num_nodes)+i] *= direct_synchronous_reactance_difference[i]
        f_values[(2*num_nodes)+i] += (voltage_reference[i] - voltage[i])
        f_values[(2*num_nodes)+i] /= transient_time_constant[i]

    return f_values


def equations_of_motion_feedback_control(state_values, double t, time_args, state_args, control_args):
    cdef int num_nodes = state_args["number of nodes"]
    mechanical_power_args = state_args["mechanical power"]
    coupling_matrix = state_args["coupling matrix"]
    voltage_reference = state_args["voltage references"]
    transient_time_constant = state_args["machine parameters"]["transient time constant"]
    damping_constant = state_args["machine parameters"]["damping constant"]
    direct_synchronous_reactance_difference = state_args["machine parameters"]["synchronous reactance difference"]
    inertia = state_args["machine parameters"]["inertia"]
    rotor_angle = state_values[:num_nodes]
    frequency_deviation = state_values[num_nodes:2*num_nodes]
    voltage = state_values[2*num_nodes:3*num_nodes]
    f_values = np.zeros(3*num_nodes, dtype=np.float64)
    cdef double rotor_angle_difference
    cdef int i, j
    for i in xrange(num_nodes):
        f_values[i] = frequency_deviation[i]
        f_values[num_nodes+i] = mechanical_power(t, mechanical_power_args)[i] + feedback_control(t, state_values, control_args)[i] - (damping_constant[i] * frequency_deviation[i])
        for j in xrange(num_nodes):
            rotor_angle_difference = rotor_angle[i] - rotor_angle[j]
            f_values[num_nodes+i] -= (coupling_matrix[i, j] * voltage[i] * voltage[j] * sin(rotor_angle_difference))
            f_values[(2*num_nodes)+i] += (coupling_matrix[i, j] * voltage[j] * cos(rotor_angle_difference))
        f_values[num_nodes+i] /= inertia[i]
        f_values[(2*num_nodes)+i] *= direct_synchronous_reactance_difference[i]
        f_values[(2*num_nodes)+i] += (voltage_reference[i] - voltage[i])
        f_values[(2*num_nodes)+i] /= transient_time_constant[i]

    return f_values


def equations_of_motion_spatial_jacobian(state_values, state_args):
    cdef int num_nodes = state_args["number of nodes"]
    mechanical_power_args = state_args["mechanical power"]
    coupling_matrix = state_args["coupling matrix"]
    inertia = state_args["machine parameters"]["inertia"]
    transient_time_constant = state_args["machine parameters"]["transient time constant"]
    damping_constant = state_args["machine parameters"]["damping constant"]
    direct_synchronous_reactance_difference = state_args["machine parameters"]["synchronous reactance difference"]

    # N = num_nodes, k=1,...N for rotor angles, k = N+1,...,2N for angular frequencies, k =2N+1,...,3N for voltages
    # construct Jacobian matrix for spatial variables J_kl = f^{k}_{x_l} for k,l =1,...,3N. row indices = EoM,
    # column indices = variables
    jacobian = np.zeros((3 * num_nodes, 3 * num_nodes), dtype=np.float64)
    # calculate entries corresponding to eom for the rotor angle
    # the eom for the rotor angle is independent of the rotor angle and voltage
    # the eom for the rotor angle depends on the frequency deviation
    jacobian[:num_nodes, num_nodes:2 * num_nodes] = np.identity(num_nodes)
    # now calculate entries corresponding to eom for the angular frequency deviation and voltages
    rotor_angle = state_values[:num_nodes]
    voltage = state_values[2 * num_nodes:3 * num_nodes]
    # entries corresponding to the angular frequencies and eom for the voltages
    cdef double rotor_angle_difference
    cdef int i, j, k
    for i in xrange(num_nodes):
        # i goes through indices for eom at each node, j goes through indices for system variables at each node
        # entries corresponding to the angular frequencies and eom for the angular frequency deviation
        jacobian[num_nodes + i, num_nodes + i] = -damping_constant[i]
        for j in xrange(num_nodes):
            if i != j:
                rotor_angle_difference = rotor_angle[i] - rotor_angle[j]
                # entries corresponding to the rotor angles and eom for the angular frequency deviation
                jacobian[num_nodes+i, j] = (coupling_matrix[i, j] * voltage[i] * voltage[j] * cos(rotor_angle_difference))
                # entries corresponding to the voltages and eom for the angular frequency deviation
                jacobian[num_nodes + i, (2 * num_nodes)+j] = -(coupling_matrix[i, j] * voltage[i] * sin(rotor_angle_difference))
                # entries corresponding to the rotor angles and eom for the voltages
                jacobian[(2 * num_nodes) + i, j] = (direct_synchronous_reactance_difference[i] / transient_time_constant[i]) * coupling_matrix[i, j] * voltage[j] * sin(rotor_angle_difference)
                # entries corresponding to the voltages and eom for the voltages
                jacobian[(2 * num_nodes) + i, (2 * num_nodes)+j] = (direct_synchronous_reactance_difference[i] / transient_time_constant[i]) * coupling_matrix[i, j] * cos(rotor_angle_difference)
            else:
                # entries corresponding to the rotor angles and eom for the voltages

                # entries corresponding to the voltages and eom for the voltages
                jacobian[(2 * num_nodes) + i, (2 * num_nodes) + i] = (((coupling_matrix[i, i] * direct_synchronous_reactance_difference[i]) -
                                                                       1.0) / transient_time_constant[i])
                for k in xrange(num_nodes):
                    # equations of motion become independent of the rotor angles when the investigating impact of node
                    # coupling to itself (i = k), furthermore if i = k then some terms are multiplied by 0 or 1 and can
                    # be simplified
                    if i != k:
                        rotor_angle_difference = rotor_angle[i] - rotor_angle[k]
                        # entries corresponding to the rotor angles and eom for the angular frequency deviation
                        jacobian[num_nodes + i, i] -= (coupling_matrix[i, k] * voltage[i] * voltage[k] * cos(rotor_angle_difference))
                        # entries corresponding to the voltages and eom for the angular frequency deviation
                        jacobian[num_nodes + i, (2 * num_nodes) + i] -= (coupling_matrix[i, k] * voltage[k] * sin(rotor_angle_difference))
                        # entries corresponding to the rotor angles and eom for the voltages
                        jacobian[(2 * num_nodes) + i, i] -= (coupling_matrix[i, k] * voltage[k] * sin(rotor_angle_difference))

                jacobian[(2 * num_nodes) + i, i] *= (direct_synchronous_reactance_difference[i] / transient_time_constant[i])
        jacobian[num_nodes + i, :] /= inertia[i]
    return jacobian


def equations_of_motion_control_derivative(int node, state_args):
    # construct derivative J^{i}_k = f^{k}_{u_{i}} for k in \{1,...,3N\} where i \in \{1,...,N} is the control at the
    # i-th node
    cdef int num_nodes = state_args["number of nodes"]
    cdef double this_inertia = state_args["machine parameters"]["inertia"][node]
    derivative = np.zeros(3 * num_nodes, dtype=np.float64)
    derivative[num_nodes + node] = 1.0 / this_inertia
    return derivative


def equations_of_motion_time_derivative(double t, state_args, mechanical_power_args):
    cdef int num_nodes = state_args["number of nodes"]
    inertia = state_args["machine parameters"]["inertia"]
    derivative = np.zeros(3 * num_nodes, dtype=np.float64)
    # time only appears appears in the EoM for the angular frequency as the mechanical power depends on it
    derivative[num_nodes:2*num_nodes][:] = np.divide(time_derivative_mechanical_power(t, mechanical_power_args), inertia)
    return derivative


def transferred_power(state_values, state_args):
    cdef int num_nodes = state_args["number of nodes"]
    coupling_matrix = state_args["coupling matrix"]
    rotor_angle = state_values[:num_nodes]
    voltage = state_values[2*num_nodes:3*num_nodes]
    transferred_power_values = np.zeros(num_nodes, dtype=np.float64)
    for i in xrange(num_nodes):
        for j in xrange(num_nodes):
            rotor_angle_difference = rotor_angle[i] - rotor_angle[j]
            transferred_power_values[i] += (coupling_matrix[i, j] * voltage[i] * voltage[j] * sin(rotor_angle_difference))
    return transferred_power_values


########################################################################################################################


def standard_logistic(double y):
    cdef double value = 1.0 / (1 + exp(-y))
    return value


def interval_indicator(double x, double a, double b, double steepness):
    cdef double indicator = standard_logistic(steepness*(x-a)) * standard_logistic(steepness*(b-x))
    return indicator


def interval_indicator_derivative(double x, double a, double b, double steepness):
    cdef double left_hand_side_indicator = standard_logistic(steepness*(x-a))
    cdef double right_hand_side_indicator = standard_logistic(steepness*(b-x))
    cdef double left_hand_side_derivative = left_hand_side_indicator * (1.0 - left_hand_side_indicator) * steepness
    cdef double right_hand_side_derivative = right_hand_side_indicator * (1.0 - right_hand_side_indicator) * (-steepness)
    cdef double derivative = (left_hand_side_derivative * right_hand_side_indicator) + (right_hand_side_derivative * left_hand_side_indicator)
    return derivative


def mechanical_power(double t, mechanical_power_args):
    return mechanical_power_methods.mechanical_power(t, mechanical_power_args)


def time_derivative_mechanical_power(double t, mechanical_power_args):
    return mechanical_power_methods.time_derivative_mechanical_power(t, mechanical_power_args)
