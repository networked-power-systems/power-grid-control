from common_imports import *
import frequency_methods_enhanced as frequency_methods_e
import frequency_methods
import constraints

########################################################################################################################
"""
Evaluation of constraint costs
"""
########################################################################################################################


def frequency_dispersion_lagrange(state_values, int num_nodes):
    frequencies = state_values[num_nodes:2 * num_nodes]
    cdef double lagrange_cost = np.var(frequencies, dtype=np.float64)
    return lagrange_cost


def frequency_dispersion_constraint_gradient(state_values, int num_nodes):
    frequencies = state_values[num_nodes:2 * num_nodes]
    constraint_gradient = np.zeros(3 * num_nodes, dtype=np.float64)
    constraint_gradient[num_nodes:2 * num_nodes] = (2.0 / num_nodes) * (frequencies - np.mean(frequencies, dtype=np.float64))
    return constraint_gradient


def frequency_dispersion_cost_from_ts(double s, state_ts, enhancing_control_args, int num_partitions, int num_nodes):
    state_values = state_ts[min(int(s*num_partitions), num_partitions)]
    cdef double f_value_time = frequency_methods_e.enhancing_control_equation_of_motion(s, *enhancing_control_args)
    cdef double cost = f_value_time * frequency_dispersion_lagrange(state_values, num_nodes)
    return cost


def frequency_dispersion_performance(state_ts, fixed_knots, enhancing_control_args, int num_partitions, int num_nodes,
                                     double mayer_weight):
    cdef double s
    cdef double performance_value = simps([frequency_dispersion_cost_from_ts(s, state_ts, enhancing_control_args,
    num_partitions, num_nodes) for s in fixed_knots], x=fixed_knots)
    performance_value += (mayer_weight * frequency_dispersion_lagrange(state_ts[-1], num_nodes))
    return performance_value

########################################################################################################################
"""
Costate variables: dp/ds = -^H_{(t,x)}(s,(t(s),x(t(s))),u(t(s)),p(s)); p(1) = M_{(x,t)}(x(t(1)),t(1)).
"""
########################################################################################################################


def frequency_dispersion_costate_eom(costate, double s, state_ts, int num_partitions, int num_nodes, state_args,
                                     enhancing_control_args):
    return -frequency_dispersion_hamiltonian_state_gradient(costate, s, state_ts, num_partitions, num_nodes, state_args,
                                                            enhancing_control_args)


def frequency_dispersion_costate_eom_backward(costate, double s, state_ts, int num_partitions, int num_nodes,
                                              state_args, enhancing_control_args):
    return -frequency_dispersion_costate_eom(costate, -s, state_ts, num_partitions, num_nodes, state_args,
                                             enhancing_control_args)


def integrate_frequency_dispersion_costate(state_ts, fixed_knots, int num_partitions, int num_nodes, state_args,
                                           enhancing_control_args, double mayer_weight):
    costate_terminal_value = mayer_weight * frequency_dispersion_constraint_gradient(state_ts[-1], num_nodes)
    costate_ts = odeint(frequency_dispersion_costate_eom_backward, costate_terminal_value, -fixed_knots[::-1],
                        args=(state_ts, num_partitions, num_nodes, state_args, enhancing_control_args),
                              tcrit=(-fixed_knots[::-1])[1:-1]
                        )[::-1]
    return costate_ts


########################################################################################################################
"""
Gradient of Hamiltonian w.r.t to space-time state variable (x,t)
"""
########################################################################################################################


def frequency_dispersion_hamiltonian_state_gradient(costate, double s, state_ts, int num_partitions, int num_nodes,
                                                    state_args, enhancing_control_args):
    cdef double f_value_time = frequency_methods_e.enhancing_control_equation_of_motion(s, *enhancing_control_args)
    state_values = state_ts[min(int(s * num_partitions), num_partitions)]
    jacobian = frequency_methods.equations_of_motion_spatial_jacobian(state_values, state_args)
    gradient_value = np.dot(costate.T, jacobian)
    gradient_value += frequency_dispersion_constraint_gradient(state_values, num_nodes)
    gradient_value *= f_value_time
    return gradient_value



########################################################################################################################
"""
Derivative of Hamiltonian w.r.t to a component of the control variable
"""
########################################################################################################################


def frequency_dispersion_hamiltonian_control_derivative(double s, state_ts, costate_ts, int num_partitions, int node,
                                                        state_args, enhancing_control_args):
    costate = costate_ts[min(int(s * num_partitions), num_partitions)]
    state_values = state_ts[min(int(s * num_partitions), num_partitions)]
    cdef double f_value_time = frequency_methods_e.enhancing_control_equation_of_motion(s, *enhancing_control_args)
    # the lagrange term is independent of the control since this is a pure state constraint
    eom_control_derivative = frequency_methods.equations_of_motion_control_derivative(node, state_args)
    cdef double derivative_value = np.dot(costate.T, eom_control_derivative) * f_value_time
    return derivative_value


def frequency_dispersion_constraint_control_parameter_gradient(state_ts, costate_ts, fixed_knots, int num_partitions,
                                                               int num_nodes, state_args, enhancing_control_args):
    # there are N = num_nodes control variables, each of which is parametrised by n_{p} = num_partitions control values
    # [u^{1},...,u^{N}], u^{i} = [u^{i}_{1},...,u^{i}_{n_{p}}]
    gradient = np.array([quad(frequency_dispersion_hamiltonian_control_derivative,
                               fixed_knots[this_partition], fixed_knots[this_partition + 1],
                               args=(state_ts, costate_ts, num_partitions, this_node, state_args, enhancing_control_args))[0]
                         for this_node in xrange(num_nodes) for this_partition in xrange(num_partitions)])
    return gradient


