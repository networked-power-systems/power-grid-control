"""
Author: Randall Martyr
Date: 3rd August 2018
This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License
(https://creativecommons.org/licenses/by-sa/4.0/).
"""

from common_imports import *
import mechanical_power_methods, control_methods
import plotting
import oc_evaluation

##########################################################################################################
# Model Initialisation
###########################################################################################################
initial_time, terminal_time = 0, 60.0
num_partitions = int(terminal_time - initial_time) * 25
num_knots = num_partitions + 1
dt = (terminal_time - initial_time) / float(num_partitions)
var_knots = np.array([initial_time + dt * N for N in xrange(num_knots)])
fixed_knots = np.arange(num_knots) / float(num_partitions)
time_args = {"number of partitions": num_partitions, "variable knots": var_knots, "fixed knots": fixed_knots,
             "terminal time": terminal_time, "decision variables": np.ones(num_partitions) * terminal_time}

num_nodes = 4

initial_rotor_angle = np.array([0.09118965, 0.09734933, 0.09303393, 0.11574618], dtype=np.float64)
initial_frequency_deviation = np.zeros(num_nodes, dtype=np.float64)
initial_voltage = np.array([0.99775135, 0.99677549, 1.00559244, 1.00719003], dtype=np.float64)
initial_load = np.array([2.0, 1.0, 1.5, 1.0], dtype=np.float64)
initial_generation = np.array([1.1, 1.4, 0.8, 2.2], dtype=np.float64)
initial_mechanical_power = initial_generation - initial_load
total_net_injection = np.sum(initial_mechanical_power)
if abs(total_net_injection) > 0.05:
    initial_mechanical_power[-1] -= total_net_injection

machine_inertia = np.array([5.22, 3.98, 4.49, 4.22], dtype=np.float64)
damping_constant = np.array([1.60, 1.22, 1.38, 1.42], dtype=np.float64)
transient_time_constant = np.array([5.54, 7.41, 6.11, 6.22], dtype=np.float64)
direct_synchronous_reactance = np.array([1.84, 1.62, 1.80, 1.94], dtype=np.float64)
direct_synchronous_transient_reactance = np.array([0.25, 0.17, 0.36, 0.44], dtype=np.float64)
direct_synchronous_reactance_difference = direct_synchronous_reactance - direct_synchronous_transient_reactance
voltage_reference = np.array([4.41, 4.20, 4.37, 4.45], dtype=np.float64)
voltage_reference = np.multiply(direct_synchronous_reactance_difference, voltage_reference)

coupling_matrix = np.diag(np.array([-49.61, -61.66, -52.17, -40.18], dtype=np.float64))

given_parameters = {(1, 4): 21.0, (1, 2): 25.6, (3, 4): 16.6, (2, 3): 33.1}

for key in given_parameters:
    i, j = key
    coupling_matrix[i - 1, j - 1] = given_parameters[key]
    coupling_matrix[j - 1, i - 1] = coupling_matrix[i - 1, j - 1]

coupling_matrix *= (4.0 / 3.0)

voltage_min, voltage_max = 0.94, 1.06
voltage_constraints = np.tile([voltage_min, voltage_max], (num_nodes, 1))

initial_state = np.concatenate((initial_rotor_angle, initial_frequency_deviation, initial_voltage))

logistic_steepness = 500.0
disturbance_type = 'persistent'
disturbance_scenarios = {'temporary': {1: ((-2.0, 10.0, 30.0 + 0.5),), },
                         'persistent': {1: ((-2.0, 10.0, terminal_time + 0.5),), },
                         'TripEtAl': {1: ((-0.2, 10.0),), 2: ((-0.05, 10.0),), 3: ((-0.05, 10.0),), 4: ((-0.1, 10.0),)}
                         }

disturbance_params = disturbance_scenarios[disturbance_type] if disturbance_type in disturbance_scenarios.keys() else dict()
mechanical_power_func = mechanical_power_methods.bump_disturbances

mechanical_power_args = {
                             "func": mechanical_power_func,
                             "args": {
                                 "initial mechanical power": initial_mechanical_power, "number of nodes": num_nodes,
                                 "disturbance parameters": disturbance_params, 'steepness': logistic_steepness
                             }
                        }

state_args = {"initial state": initial_state, "number of nodes": num_nodes, "mechanical power": mechanical_power_args,
              "coupling matrix": coupling_matrix, "voltage references": voltage_reference,
              "machine parameters": {"transient time constant": transient_time_constant,
                                     "damping constant": damping_constant,
                                     "synchronous reactance difference": direct_synchronous_reactance_difference,
                                     "inertia": machine_inertia}}


lagrange_quadratic_cost_parameter = 1.0
diagonal = np.tile([lagrange_quadratic_cost_parameter], num_nodes)
lagrange_quadratic_cost_matrix = np.diag(diagonal)
oc_args = {
           "objective": (lagrange_quadratic_cost_matrix,),
           "constraints": {"terminal time": terminal_time},
           "mayer weights": {
                "frequency dispersion": 1.0, "voltage": 1.0, "frequency deviation": 1.0
           },
           "tolerance": {
                         "frequency dispersion": 10**-5, "voltage": 10**-5, "frequency deviation": 10**-5,
                         "controlled power": 10**-10
           }
           }

# constraints
# voltage constraints
oc_args["constraints"]["voltage"] = voltage_constraints

# nominal frequency in UK is 50 Hz, operational limits are 49.8 Hz to 50.2 Hz, remember to multiply 2.0 * math.pi
# statutory limits are 49.5 Hz to 50.5 Hz
nominal = 50.0
max_deviation_proportion = 10**-3
frequency_deviation_constraints = np.array([-nominal*max_deviation_proportion, nominal*max_deviation_proportion], dtype=np.float64) * 2.0 * math.pi
oc_args["constraints"]["frequency deviation"] = frequency_deviation_constraints
u_min, u_max = -5.0, 5.0
oc_args["constraints"]["controlled power"] = np.tile([u_min, u_max], (num_nodes, 1))
controlled_power_contraints = {"min": np.tile([u_min], num_nodes), "max": np.tile([u_max], num_nodes)}

optimisation_args = (time_args, state_args, oc_args)
parameter_bounds = [(u_min, u_max)] * (num_partitions * num_nodes)


save_plot = False
include_legend = False
print_result = True
write_result = False
skip = 1
max_control_param = 100.0

doLinearFrequency, doIntegralFrequency, doLinearAngle, doGABFrequency = False, True, False, True
doCheckLLFControlParam, doCheckILFControlParam, doCheckLLAControlParam, doCheckGABControlParam = False, False, False, False

############################
# Linear Frequency
#############################


if doLinearFrequency:

    start_control_param = 0.0
    given_control_param = 1.0

    prefix = disturbance_type + '-linear_frequency-' + str(num_nodes) + '_nodes'
    print "\nLinear Frequency.\n"
    control_args_array = {
        "label": "linear frequency",
        "primary axis": True,
        "feedback": True,
        "args": {
            "func": control_methods.linear_frequency,
            "args": {"number of nodes": num_nodes,
                     "control constraints": controlled_power_contraints}
        }
    }
    control_param_matrix = np.zeros((num_nodes, num_nodes), dtype=np.float64)
    if doCheckLLFControlParam:
        print "Iteration over control parameter space...\n"
        result_array = []
        setControlParam = False
        control_param = start_control_param
        doLoop = True
        while doLoop:
            np.fill_diagonal(control_param_matrix, control_param)
            control_args_array["args"]["args"]["coefficient matrix"] = control_param_matrix.copy()
            result = oc_evaluation.evaluate_oc(time_args, state_args, control_args_array, oc_args, print_result=False,
                                               write_result=False)

            if (not setControlParam) and np.all(np.less(result[1:], [oc_args["tolerance"]["frequency dispersion"],
                                              oc_args["tolerance"]["frequency deviation"],
                                              oc_args["tolerance"]["voltage"], oc_args["tolerance"]["controlled power"]])
                                              ):
                setControlParam = True
                doLoop = False

            result_array.append(result)
            control_param += skip
            doLoop = doLoop and (control_param <= max_control_param)

        print "...done!\n"
        control_param -= skip
        print "Linear frequency control parameter:", control_param
        result_array = np.array(result_array)
    else:
        control_param = given_control_param

    np.fill_diagonal(control_param_matrix, control_param)
    control_args_array["args"]["args"]["coefficient matrix"] = control_param_matrix.copy()

    controls = list()
    controls.append(control_args_array)

    oc_evaluation.evaluate_oc(time_args, state_args, control_args_array, oc_args, print_result=print_result,
                              write_result=write_result, save_folder='../results/evaluation/', prefix=prefix)

    plotting.solutionPlot(time_args, state_args, controls, oc_args, save_plot=save_plot, legend=include_legend,
                          save_folder='../results/plots/', prefix=prefix)


#########################
# Linear Rotor Angle
#########################

if doLinearAngle:

    start_control_param = 0.0
    given_control_param = 100.0

    prefix = disturbance_type + '-linear_angle-' + str(num_nodes) + '_nodes'
    print "\nLinear Angle.\n"
    control_args_array = {
        "label": "linear angle",
        "primary axis": True,
        "feedback": True,
        "args": {
            "func": control_methods.linear_rotor_angle,
            "args": {"number of nodes": num_nodes,
                     "rotor angle initial value": initial_rotor_angle.copy(),
                     "control constraints": controlled_power_contraints}
        }
    }
    control_param_matrix = np.zeros((num_nodes, num_nodes), dtype=np.float64)
    if doCheckLLAControlParam:
        print "Iteration over control parameter space...\n"
        result_array = []
        setControlParam = False
        control_param = start_control_param
        doLoop = True
        while doLoop:
            np.fill_diagonal(control_param_matrix, control_param)
            control_args_array["args"]["args"]["coefficient matrix"] = control_param_matrix.copy()
            result = oc_evaluation.evaluate_oc(time_args, state_args, control_args_array, oc_args, print_result=False,
                                               write_result=False)

            if (not setControlParam) and np.all(np.less(result[1:], [oc_args["tolerance"]["frequency dispersion"],
                                              oc_args["tolerance"]["frequency deviation"],
                                              oc_args["tolerance"]["voltage"], oc_args["tolerance"]["controlled power"]])
                                              ):
                setControlParam = True
                doLoop = False

            result_array.append(result)
            control_param += skip
            doLoop = doLoop and (control_param <= max_control_param)

        print "...done!\n"
        control_param -= skip
        print "Linear angle control parameter:", control_param
        result_array = np.array(result_array)
    else:
        control_param = given_control_param
    np.fill_diagonal(control_param_matrix, control_param)
    control_args_array["args"]["args"]["coefficient matrix"] = control_param_matrix.copy()

    controls = list()
    controls.append(control_args_array)

    oc_evaluation.evaluate_oc(time_args, state_args, control_args_array, oc_args, print_result=print_result,
                              write_result=write_result, save_folder='../results/evaluation/', prefix=prefix)

    plotting.solutionPlot(time_args, state_args, controls, oc_args, save_plot=save_plot, legend=include_legend,
                          save_folder='../results/plots/', prefix=prefix)

############################
# Integral Frequency
#############################

if doIntegralFrequency:

    start_control_param = 0.0
    given_control_param = 1.0 / 1.0

    prefix = disturbance_type + '-integral_frequency-' + str(num_nodes) + '_nodes'
    print "\nIntegral Frequency.\n"

    # trapezoidal rule is a convex combination of the old and new angular velocities
    # with old_frequency_coefficient = 0.5, let's try different values
    old_angular_velocity_coefficient = 0.5
    
    control_args_array = {
        "label": "integral frequency",
        "is integral": True,
        "primary axis": True,
        "feedback": True,
        "args": {
            "func": control_methods.integral_frequency,
            "args": {"number of nodes": num_nodes,
                     "old angular velocity": np.zeros(num_nodes, dtype=np.float64),
                     "old total": np.zeros(num_nodes, dtype=np.float64),
                     "old angular velocity coefficient": old_angular_velocity_coefficient,
                     "old time": 0.0, "control constraints": controlled_power_contraints}
        }
    }
    
    control_param_matrix = np.zeros((num_nodes, num_nodes), dtype=np.float64)
    if doCheckILFControlParam:
        print "Iteration over control parameter space...\n"
        result_array = []
        setControlParam = False
        control_param = start_control_param
        doLoop = True
        while doLoop:
            np.fill_diagonal(control_param_matrix, control_param)
            control_args_array["args"]["args"]["coefficient matrix"] = control_param_matrix.copy()
            result = oc_evaluation.evaluate_oc(time_args, state_args, control_args_array, oc_args, print_result=False,
                                               write_result=False)

            if (not setControlParam) and np.all(np.less(result[1:], [oc_args["tolerance"]["frequency dispersion"],
                                              oc_args["tolerance"]["frequency deviation"],
                                              oc_args["tolerance"]["voltage"], oc_args["tolerance"]["controlled power"]])
                                              ):
                setControlParam = True
                doLoop = False

            result_array.append(result)
            control_param += skip
            doLoop = doLoop and (control_param <= max_control_param)

        print "...done!\n"
        control_param -= skip
        print "Integral frequency control parameter:", control_param
        result_array = np.array(result_array)
    else:
        control_param = given_control_param

    np.fill_diagonal(control_param_matrix, control_param)
    control_args_array["args"]["args"]["coefficient matrix"] = control_param_matrix.copy()

    controls = list()
    controls.append(control_args_array)

    oc_evaluation.evaluate_oc(time_args, state_args, control_args_array, oc_args, print_result=print_result,
                              write_result=write_result, save_folder='../results/evaluation/', prefix=prefix)

    plotting.solutionPlot(time_args, state_args, controls, oc_args, save_plot=save_plot, legend=include_legend,
                          save_folder='../results/plots/', prefix=prefix)


############################
# Gather and Broadcast Frequency
#############################

if doGABFrequency:

    start_control_param = 0.0
    given_control_param = 1.0 / 4.0

    prefix = disturbance_type + '-GAB_frequency-' + str(num_nodes) + '_nodes'
    print "\nGAB Frequency.\n"

    # trapezoidal rule is a convex combination of the old and new angular velocities
    # with old_frequency_coefficient = 0.5, let's try different values
    old_angular_velocity_coefficient = 0.5

    control_args_array = {
        "label": "gather and broadcast",
        "is integral": True,
        "primary axis": True,
        "feedback": True,
        "args": {
            "func": control_methods.gather_and_broadcast,
            "args": {"number of nodes": num_nodes,
                     "old angular velocity": np.zeros(num_nodes, dtype=np.float64),
                     "old total": np.zeros(num_nodes, dtype=np.float64),
                     "old angular velocity coefficient": old_angular_velocity_coefficient,
                     "old time": 0.0, "control constraints": controlled_power_contraints}
        }
    }

    control_param_matrix = np.zeros((num_nodes, num_nodes), dtype=np.float64)
    if doCheckGABControlParam:
        print "Iteration over control parameter space...\n"
        result_array = []
        setControlParam = False
        control_param = start_control_param
        doLoop = True
        while doLoop:
            control_param_matrix.fill(control_param)
            control_args_array["args"]["args"]["coefficient matrix"] = control_param_matrix.copy()
            result = oc_evaluation.evaluate_oc(time_args, state_args, control_args_array, oc_args, print_result=False,
                                               write_result=False)
            if (not setControlParam) and np.all(np.less(result[1:], [oc_args["tolerance"]["frequency dispersion"],
                                                                     oc_args["tolerance"]["frequency deviation"],
                                                                     oc_args["tolerance"]["voltage"],
                                                                     oc_args["tolerance"]["controlled power"]])
                                                ):
                setControlParam = True
                doLoop = False

            result_array.append(result)
            control_param += skip
            doLoop = doLoop and (control_param <= max_control_param)

        print "...done!\n"
        control_param -= skip
        print "GAB frequency control parameter:", control_param
        result_array = np.array(result_array)
    else:
        control_param = given_control_param
    control_param_matrix.fill(control_param)
    control_args_array["args"]["args"]["coefficient matrix"] = control_param_matrix.copy()

    controls = list()
    controls.append(control_args_array)

    oc_evaluation.evaluate_oc(time_args, state_args, control_args_array, oc_args, print_result=print_result,
                              write_result=write_result, save_folder='../results/evaluation/', prefix=prefix)

    plotting.solutionPlot(time_args, state_args, controls, oc_args, save_plot=save_plot, legend=include_legend,
                          save_folder='../results/plots/', prefix=prefix)