from common_imports import *
import frequency_methods_enhanced as frequency_methods_e
import frequency_methods

# note to multiply lagrange performance values by f_value_time = v(s)
# mainly methods for evaluating the performance of a control, including the constraint costs


def lagrange_cost(control_values, a_matrix):
    cdef double cost = np.dot(control_values.T, np.dot(a_matrix, control_values))
    return cost

def lagrange_cost_helper(double s, control_args, enhancing_control_args, *lagrange_args):
    cdef double f_value_time = frequency_methods_e.enhancing_control_equation_of_motion(s, *enhancing_control_args)
    control_values = frequency_methods_e.control(s, control_args, enhancing_control_args)
    cdef double cost = f_value_time * lagrange_cost(control_values, *lagrange_args)
    return cost


def performance_criterion(fixed_knots, int num_partitions, control_args, enhancing_control_args, lagrange_args):
    cdef double s
    cdef double lagrange_value = simps([lagrange_cost_helper(s, control_args, enhancing_control_args, lagrange_args) for s in fixed_knots], x=fixed_knots)
    return lagrange_value


def lagrange_control_derivative(control_values, int node, a_matrix):
    # gradient of quadratic form x'Ax = (A + A')x = 2Ax since A is symmetric
    # i-th component of gradient = 2A_{i,:}x where A_{i,:} is the i-th row of the matrix A.
    cdef double derivative = 2.0 * np.dot(a_matrix[node, :], control_values)
    return derivative


########################################################################################################################
"""
Costate variables: dp/ds = -^H_{(t,x)}(s,(t(s),x(t(s))),u(t(s)),p(s)); p(1) = M_{(x,t)}(x(t(1)),t(1)).
"""
########################################################################################################################


def performance_costate_eom(costate, double s, state_ts, int num_partitions, state_args, enhancing_control_args):
    return -performance_hamiltonian_state_gradient(costate, s, state_ts, num_partitions, state_args,
                                                   enhancing_control_args)


def performance_costate_eom_backward(costate, double s, state_ts, int num_partitions, state_args,
                                     enhancing_control_args):
    return -performance_costate_eom(costate, -s, state_ts, num_partitions, state_args, enhancing_control_args)


def integrate_performance_costate(state_ts, fixed_knots, int num_partitions, state_args, enhancing_control_args):
    costate_terminal_value = np.zeros_like(state_ts[-1])
    costate_ts = odeint(performance_costate_eom_backward, costate_terminal_value, -fixed_knots[::-1],
                        args=(state_ts, num_partitions, state_args, enhancing_control_args),
                        tcrit=(-fixed_knots[::-1])[1:-1]
                        )[::-1]
    return costate_ts

########################################################################################################################
"""
Gradient of Hamiltonian w.r.t to space-time state variable (x,t)
"""
########################################################################################################################


def performance_hamiltonian_state_gradient(costate, double s, state_ts, int num_partitions, state_args,
                                           enhancing_control_args):
    state_values = state_ts[min(int(s * num_partitions), num_partitions)]
    jacobian = frequency_methods.equations_of_motion_spatial_jacobian(state_values, state_args)
    cdef double f_value_time = frequency_methods_e.enhancing_control_equation_of_motion(s, *enhancing_control_args)
    # lagrange cost is purely control
    gradient_value = np.dot(costate.T, jacobian) * f_value_time
    return gradient_value


########################################################################################################################
"""
Derivative of Hamiltonian w.r.t to a component of the control variable
"""
########################################################################################################################


def performance_hamiltonian_control_derivative(double s, costate_ts, int num_partitions, int node, state_args,
                                               control_args, enhancing_control_args, lagrange_args):
    costate = costate_ts[min(int(s * num_partitions), num_partitions)]
    control_values = frequency_methods_e.control(s, control_args, enhancing_control_args)
    cdef double f_value_time = frequency_methods_e.enhancing_control_equation_of_motion(s, *enhancing_control_args)
    cdef double derivative_value = lagrange_control_derivative(control_values, node, *lagrange_args)
    eom_control_derivative = frequency_methods.equations_of_motion_control_derivative(node, state_args)
    derivative_value += np.dot(costate.T, eom_control_derivative)
    derivative_value *= f_value_time
    return derivative_value


# get the gradient of the performance criterion w.r.t. the control parameters
# it is a combination of 5.8 in Teo (2005), with simplification given by p.142 of Teo et al
def performance_control_parameter_gradient(costate_ts, fixed_knots, int num_partitions, int num_nodes,
                                           state_args, control_args, enhancing_control_args, lagrange_args):
    # there are N = num_nodes control variables, each of which is parametrised by n_{p} = num_partitions control values
    # [u^{1},...,u^{N}], u^{i} = [u^{i}_{1},...,u^{i}_{n_{p}}]
    cdef int this_node, this_partition
    gradient = np.array([quad(performance_hamiltonian_control_derivative,
                              fixed_knots[this_partition], fixed_knots[this_partition+1],
                              args=(costate_ts, num_partitions, this_node, state_args, control_args,
                                    enhancing_control_args, lagrange_args))[0]
                         for this_node in xrange(num_nodes) for this_partition in xrange(num_partitions)])
    return gradient