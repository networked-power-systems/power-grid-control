from common_imports import *
import frequency_methods
cimport cython
from libc.math cimport exp


def integrate_state(fixed_knots, time_args, state_args, control_args, enhancing_control_args, bint feedback = False):
    initial_state = state_args["initial state"]
    if feedback:
        return odeint(equations_of_motion_feedback_control, initial_state, fixed_knots, tcrit=fixed_knots[1:-1],
                          args=(time_args, state_args, control_args, enhancing_control_args))
    else:
        return odeint(equations_of_motion, initial_state, fixed_knots, tcrit=fixed_knots[1:-1], args=(time_args,
                          state_args, control_args, enhancing_control_args))

def equations_of_motion(state_values, double s, time_args, state_args, control_args, enhancing_control_args):
    cdef double time_value = exact_time_transform(s, *enhancing_control_args)
    cdef double f_value_time = enhancing_control_equation_of_motion(s, *enhancing_control_args)
    f_values = f_value_time * frequency_methods.equations_of_motion(state_values, time_value, time_args, state_args, control_args)
    return f_values


def equations_of_motion_feedback_control(state_values, double s, time_args, state_args, control_args, enhancing_control_args):
    cdef double time_value = exact_time_transform(s, *enhancing_control_args)
    cdef double f_value_time = enhancing_control_equation_of_motion(s, *enhancing_control_args)
    f_values = f_value_time * frequency_methods.equations_of_motion_feedback_control(state_values, time_value, time_args, state_args, control_args)
    return f_values


def control(double s, control_args, enhancing_control_args):
    cdef double time_value = exact_time_transform(s, *enhancing_control_args)
    control_value = frequency_methods.control(time_value, *control_args)
    return control_value


def terminal_time_transform(fixed_knots, ec_variables):
    transformed_terminal_time = np.dot(ec_variables, np.diff(fixed_knots))
    return transformed_terminal_time


def exact_time_transform(double s, ec_variables, fixed_knots, int num_partitions):
    cdef double t_s
    cdef int knot_index
    if s == 1.0:
        t_s = terminal_time_transform(fixed_knots, ec_variables)
    else:
        knot_index = int(s*num_partitions)
        t_s = (s - fixed_knots[knot_index]) * ec_variables[knot_index] if knot_index < num_partitions else 0.0
        if knot_index > 0:
            t_s += (np.dot(ec_variables[:knot_index], np.diff(fixed_knots)[:knot_index]))
    return t_s


def enhancing_control_equation_of_motion(double s, ec_variables, fixed_knots, int num_partitions):
    cdef int knot_index = int(s*num_partitions)
    cdef double enhanced_control_value = ec_variables[knot_index] if knot_index < num_partitions else 0.0
    return enhanced_control_value


def standard_logistic(double y):
    cdef double value = 1.0 / (1 + exp(-y))
    return value


def interval_indicator(double x, double a, double b, double steepness):
    cdef double indicator = standard_logistic(steepness*(x-a)) * standard_logistic(steepness*(b-x))
    return indicator


def interval_indicator_derivative(double x, double a, double b, double steepness):
    cdef double left_hand_side_indicator = standard_logistic(steepness*(x-a))
    cdef double right_hand_side_indicator = standard_logistic(steepness*(b-x))
    cdef double left_hand_side_derivative = left_hand_side_indicator * (1.0 - left_hand_side_indicator) * steepness
    cdef double right_hand_side_derivative = right_hand_side_indicator * (1.0 - right_hand_side_indicator) * (-steepness)
    cdef double derivative = (left_hand_side_derivative * right_hand_side_indicator) + (right_hand_side_derivative * left_hand_side_indicator)
    return derivative


def equations_of_motion_jacobian(state_values, double s, state_args, enhancing_control_args):
    cdef double f_value_time = enhancing_control_equation_of_motion(s, *enhancing_control_args)
    jacobian = frequency_methods.equations_of_motion_spatial_jacobian(state_values, state_args) * f_value_time
    return jacobian


def equations_of_motion_control_derivative(double s, state_args, enhancing_control_args, int node):
    cdef double f_value_time = enhancing_control_equation_of_motion(s, *enhancing_control_args)
    control_derivative = frequency_methods.equations_of_motion_control_derivative(node, state_args) * f_value_time
    return control_derivative
