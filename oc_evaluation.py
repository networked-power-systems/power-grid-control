from common_imports import *

import frequency_methods_enhanced, frequency_methods
import control_methods
import performance_methods
import frequency_deviation_constraint_methods
import frequency_dispersion_constraint_methods
import voltage_constraint_methods
import controlled_power_constraint_methods


def evaluate_oc(time_args, state_args, control_args, oc_args, print_result=True, write_result=False,
                save_folder='./', prefix=''):
    num_partitions = time_args["number of partitions"]
    var_knots = time_args["variable knots"]
    fixed_knots = time_args["fixed knots"]
    num_nodes = state_args["number of nodes"]
    ec_variables = time_args["decision variables"]
    terminal_time = time_args["terminal time"]
    enhancing_control_args = (ec_variables, fixed_knots, num_partitions)

    integral_controller = "is integral" in control_args.keys() and control_args["is integral"]

    if integral_controller:
        control_args["args"]["args"]["old angular velocity"] = np.zeros(num_nodes, dtype=np.float64)
        control_args["args"]["args"]["old total"] = np.zeros(num_nodes, dtype=np.float64)
        control_args["args"]["args"]["old time"] = 0.0

    state_ts = frequency_methods_enhanced.integrate_state(fixed_knots, time_args, state_args, control_args["args"],
                                                          enhancing_control_args, feedback=control_args["feedback"])

    if integral_controller:
        control_args["args"]["args"]["old angular velocity"] = np.zeros(num_nodes, dtype=np.float64)
        control_args["args"]["args"]["old total"] = np.zeros(num_nodes, dtype=np.float64)
        control_args["args"]["args"]["old time"] = 0.0

    if control_args["feedback"]:

        control_parameters = np.array([frequency_methods.feedback_control(var_knots[index], state_ts[index, :],
                                       control_args["args"]) for index in xrange(num_partitions)])

        if integral_controller:
            # need to reset integral controller
            control_args["args"]["args"]["old angular velocity"] = np.zeros(num_nodes, dtype=np.float64)
            control_args["args"]["args"]["old total"] = np.zeros(num_nodes, dtype=np.float64)
            control_args["args"]["args"]["old time"] = 0.0

        open_loop_control_args = {
            "func": control_methods.piecewise_constant,
            "args": {"parameters": control_parameters, "number of partitions": num_partitions,
                     "number of nodes": num_nodes, "terminal time": terminal_time}
        }
        # re-run system dynamics with open loop control
        state_ts = frequency_methods_enhanced.integrate_state(fixed_knots, time_args, state_args,
                                                              open_loop_control_args,
                                                              enhancing_control_args, feedback=True
                                                              )
    else:
        control_parameters = np.array([frequency_methods.control(var_knots[index], *control_args["args"]) for index
                               in xrange(num_partitions)])

    open_loop_control_args = (control_parameters, num_partitions, num_nodes, terminal_time)
    performance_args = oc_args["objective"]
    performance_value = performance_methods.performance_criterion(fixed_knots, num_partitions, open_loop_control_args, enhancing_control_args, *performance_args)

    if print_result:
        print "Original control performance: %g" % performance_value

    fdisp_weight = oc_args["mayer weights"]["frequency dispersion"]

    frequency_dispersion_value = frequency_dispersion_constraint_methods.frequency_dispersion_performance(state_ts, fixed_knots, enhancing_control_args,
                                                                                                          num_partitions, num_nodes, fdisp_weight)
    if print_result:
        print "Original frequency dispersion constraint: %g" % frequency_dispersion_value

    frequency_deviation_constraint = oc_args["constraints"]["frequency deviation"]
    fdev_weight = oc_args["mayer weights"]["frequency deviation"]
    frequency_deviation_value = frequency_deviation_constraint_methods.mean_frequency_deviation_performance(state_ts, fixed_knots, enhancing_control_args,
                                                                                 num_partitions, num_nodes,
                                                                                 frequency_deviation_constraint,
                                                                                 fdev_weight)
    if print_result:
        print "Original frequency deviation constraint: %g" % frequency_deviation_value

    voltage_constraint = oc_args["constraints"]["voltage"]
    voltage_weight = oc_args["mayer weights"]["voltage"]
    voltage_values = []
    if print_result:
        print "Original voltage constraint"
    for node in xrange(num_nodes):
        voltage_performance = voltage_constraint_methods.voltage_performance(node, state_ts, fixed_knots, enhancing_control_args, num_partitions,
                                                              num_nodes, voltage_constraint[node], voltage_weight)
        if print_result:
            print "At node %u: %g" % (node + 1, voltage_performance)
        voltage_values.append(voltage_performance)

    controlled_power_constraint = oc_args["constraints"]["controlled power"]
    controlled_power_values = []
    if print_result:
        print "Original controlled power constraint"
    for node in xrange(num_nodes):
        controlled_power_performance = controlled_power_constraint_methods.controlled_power_performance(node, fixed_knots, open_loop_control_args, enhancing_control_args, num_partitions,
                                                                                                        num_nodes, controlled_power_constraint[node])
        if print_result:
            print "At node %u: %g" % (node + 1, controlled_power_performance)
        controlled_power_values.append(controlled_power_performance)
        
    file_data = [performance_value, frequency_dispersion_value, frequency_deviation_value, np.max(voltage_values), np.max(controlled_power_values)]

    if write_result:
        header = 'control cost,frequency dispersion constraint,frequency deviation constraint,max voltage constraint,max controlled power constraint'
        np.savetxt(save_folder + prefix + 'evaluation.csv', file_data, delimiter=',', header=header)

    return file_data