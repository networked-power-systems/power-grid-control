"""
Author: Randall Martyr
Date: 3rd August 2018
This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License
(https://creativecommons.org/licenses/by-sa/4.0/).
"""

from common_imports import *
import cpet_methods, mechanical_power_methods, control_methods
from scipy.optimize import minimize
import datetime, time


##########################################################################################################
# Model Initialisation
###########################################################################################################
initial_time, terminal_time = 0, 60.0
num_partitions = int(terminal_time - initial_time) * 16
num_knots = num_partitions + 1
dt = (terminal_time - initial_time) / float(num_partitions)
var_knots = np.array([initial_time + dt * N for N in xrange(num_knots)])
fixed_knots = np.arange(num_knots) / float(num_partitions)
time_args = {"number of partitions": num_partitions, "variable knots": var_knots, "fixed knots": fixed_knots,
             "terminal time": terminal_time, "decision variables": np.ones(num_partitions) * terminal_time}

num_nodes = 4

initial_rotor_angle = np.array([0.09118965, 0.09734933, 0.09303393, 0.11574618], dtype=np.float64)
initial_frequency_deviation = np.zeros(num_nodes, dtype=np.float64)
initial_voltage = np.array([0.99775135, 0.99677549, 1.00559244, 1.00719003], dtype=np.float64)
initial_load = np.array([2.0, 1.0, 1.5, 1.0], dtype=np.float64)
initial_generation = np.array([1.1, 1.4, 0.8, 2.2], dtype=np.float64)
initial_mechanical_power = initial_generation - initial_load
total_net_injection = np.sum(initial_mechanical_power)
if abs(total_net_injection) > 0.05:
    initial_mechanical_power[-1] -= total_net_injection

machine_inertia = np.array([5.22, 3.98, 4.49, 4.22], dtype=np.float64)
damping_constant = np.array([1.60, 1.22, 1.38, 1.42], dtype=np.float64)
transient_time_constant = np.array([5.54, 7.41, 6.11, 6.22], dtype=np.float64)
direct_synchronous_reactance = np.array([1.84, 1.62, 1.80, 1.94], dtype=np.float64)
direct_synchronous_transient_reactance = np.array([0.25, 0.17, 0.36, 0.44], dtype=np.float64)
direct_synchronous_reactance_difference = direct_synchronous_reactance - direct_synchronous_transient_reactance
voltage_reference = np.array([4.41, 4.20, 4.37, 4.45], dtype=np.float64)
voltage_reference = np.multiply(direct_synchronous_reactance_difference, voltage_reference)

coupling_matrix = np.diag(np.array([-49.61, -61.66, -52.17, -40.18], dtype=np.float64))

given_parameters = {(1, 4): 21.0, (1, 2): 25.6, (3, 4): 16.6, (2, 3): 33.1}

for key in given_parameters:
    i, j = key
    coupling_matrix[i - 1, j - 1] = given_parameters[key]
    coupling_matrix[j - 1, i - 1] = coupling_matrix[i - 1, j - 1]

coupling_matrix *= (4.0 / 3.0)

voltage_min, voltage_max = 0.94, 1.06
voltage_constraints = np.tile([voltage_min, voltage_max], (num_nodes, 1))

initial_state = np.concatenate((initial_rotor_angle, initial_frequency_deviation, initial_voltage))

logistic_steepness = 500.0
disturbance_type = 'persistent'

disturbance_scenarios = {'temporary': {1: ((-2.0, 10.0, 30.0 + 0.5),), },
                         'persistent': {1: ((-2.0, 10.0, terminal_time + 0.5),), },
                         'TripEtAl': {1: ((-0.2, 10.0),), 2: ((-0.05, 10.0),), 3: ((-0.05, 10.0),), 4: ((-0.1, 10.0),)}
                         }

disturbance_params = disturbance_scenarios[disturbance_type] if disturbance_type in disturbance_scenarios.keys() else dict()
mechanical_power_func = mechanical_power_methods.bump_disturbances

mechanical_power_args = {
                             "func": mechanical_power_func,
                             "args": {
                                 "initial mechanical power": initial_mechanical_power, "number of nodes": num_nodes,
                                 "disturbance parameters": disturbance_params, 'steepness': logistic_steepness
                             }
                        }

state_args = {"initial state": initial_state, "number of nodes": num_nodes, "mechanical power": mechanical_power_args,
              "coupling matrix": coupling_matrix, "voltage references": voltage_reference,
              "machine parameters": {"transient time constant": transient_time_constant,
                                     "damping constant": damping_constant,
                                     "synchronous reactance difference": direct_synchronous_reactance_difference,
                                     "inertia": machine_inertia}}


lagrange_quadratic_cost_parameter = 1.0
diagonal = np.tile([lagrange_quadratic_cost_parameter], num_nodes)
lagrange_quadratic_cost_matrix = np.diag(diagonal)
oc_args = {
           "objective": (lagrange_quadratic_cost_matrix,),
           "constraints": {"terminal time": terminal_time},
           "mayer weights": {
                "frequency dispersion": 1.0, "voltage": 1.0, "frequency deviation": 1.0
           },
           "tolerance": {
                         "frequency dispersion": 10**-4, "voltage": 10**-10, "frequency deviation": 10**-10,
                         "controlled power": 10**-10
           }
           }

# constraints
# voltage constraints
oc_args["constraints"]["voltage"] = voltage_constraints

# nominal frequency in UK is 50 Hz, operational limits are 49.8 Hz to 50.2 Hz, remember to multiply 2.0 * math.pi
# statutory limits are 49.5 Hz to 50.5 Hz
nominal = 50.0
max_deviation_proportion = 10**-3
frequency_deviation_constraints = np.array([-nominal*max_deviation_proportion, nominal*max_deviation_proportion], dtype=np.float64) * 2.0 * math.pi
oc_args["constraints"]["frequency deviation"] = frequency_deviation_constraints
u_min, u_max = -5.0, 5.0
oc_args["constraints"]["controlled power"] = np.tile([u_min, u_max], (num_nodes, 1))

optimisation_args = (time_args, state_args, oc_args)
parameter_bounds = [(u_min, u_max)] * (num_partitions * num_nodes)

##########################################################################################################
# Optimisation part
##########################################################################################################

doSimulation = True
doOptimise = True
doNullInit = False
inputFile = ''

##############################
# Present optimisation results
##############################
evaluate_result = True
write_result = True
print_result = True
plot_result = True
save_plot = True


if doSimulation:

    prefix = str(datetime.date.today()) + '_' + disturbance_type + '-controlled-' + str(num_nodes) + '_nodes'

    if doNullInit:
        initial_parameters = np.zeros(num_partitions * num_nodes, dtype=np.float64)
        prefix = 'null_init-' + prefix
    else:
        if inputFile:
            path = '../results/evaluation/' + inputFile
            f_handle = open(path, 'r')
            initial_parameters = np.load(f_handle)
            initial_parameters = control_methods.resample_parameters(initial_parameters, terminal_time,
                                                                     num_nodes * num_partitions)
            f_handle.close()
        else:
            initial_parameters = np.array([initial_mechanical_power - mechanical_power_methods.mechanical_power(t, mechanical_power_args) for t in var_knots[:-1]]).flatten('F')

    initial_parameters = np.clip(initial_parameters, u_min, u_max)
        
    optimal_parameters = initial_parameters.copy()
    if doOptimise:
        print "\n Performing optimisation..."
        start = time.time()
        optimisation_result = minimize(cpet_methods.objective, initial_parameters, bounds=parameter_bounds,
                                       args=optimisation_args, method='SLSQP',
                                       jac=cpet_methods.objective_gradient,
                                       # tol=10**-8,
                                       constraints={
                                                    'type': 'ineq', 'fun': cpet_methods.ieq_constraints,
                                                    'jac': cpet_methods.ieq_constraints_grad,
                                                    'args': optimisation_args
                                       },
                                       # options={'maxiter': 150}
                                       )

        end = time.time()
        print "Done after %f seconds!" % (end - start)
        optimal_parameters = optimisation_result.x
        print "Termination message:", optimisation_result.message
        handle = open('../results/evaluation/'+prefix+'parameters.file', 'w')
        np.save(handle, optimal_parameters)
        handle.close()

    optimal_parameters = np.array(np.split(optimal_parameters, num_nodes)).T

    oc_control_args = {"label": "optimized", "primary axis": True, "feedback": False,
                       "args": (optimal_parameters, num_partitions, num_nodes, terminal_time)
                      }

    if evaluate_result:
        import oc_evaluation
        oc_evaluation.evaluate_oc(time_args, state_args, oc_control_args, oc_args, print_result=print_result,
                                  write_result=write_result, save_folder='../results/evaluation/', prefix=prefix)

    if plot_result:
        import plotting
        controls = list()
        controls.append(oc_control_args)

        plotting.solutionPlot(time_args, state_args, controls, oc_args, nominal_frequency=50.0, save_plot=save_plot,
                              legend=False, save_folder='../results/plots/', prefix=prefix)
