from common_imports import *
import frequency_methods
cimport cython


def resample_parameters(parameters, double end_time, int num_partitions):
    # convert parameters from an array of values on a regular partition of [0, end_time] to another
    # array of values on a regular partition of [0, end_time] with num_partitions sub-intervals.
    cdef int num_params = len(parameters)  # get number of parameters in the original reference array
    if num_params == num_partitions:
        return parameters

    reference_parameters = np.zeros(num_partitions)  # new array of num_partitions reference parameters
    cdef int start, end, pos
    cdef double t, dt
    t = 0.0
    dt = end_time / float(num_partitions)
    pos = 0
    while pos < num_partitions:
        # find all elements in parameters belonging to the sub-interval [t, t+dt)
        start = min(int(num_params * (t / end_time)), num_params - 1)
        end = min(int(num_params * ((t + dt) / end_time)), num_params)
        # resample using the average
        reference_parameters[pos] = np.mean(parameters[start:max(end,start+1)])
        t += dt
        pos += 1

    return reference_parameters


def exact(double t, state, control_args):
    return control_args["mechanical power"]["disturbance func"](t, control_args["mechanical power"]["args"])


def linear_frequency(double t, state, control_args):
    c_mat = -control_args["coefficient matrix"]
    cdef int num_nodes = control_args["number of nodes"]
    angular_velocity = state[num_nodes:2*num_nodes]
    control_value = np.dot(c_mat, angular_velocity)
    control_value = np.clip(control_value, control_args["control constraints"]["min"],
                            control_args["control constraints"]["max"])
    return control_value


def integral_frequency(double t, state, control_args):
    c_mat = -control_args["coefficient matrix"]
    cdef int num_nodes = control_args["number of nodes"]
    cdef double old_time = control_args["old time"]
    old_angular_velocity = control_args["old angular velocity"]
    cdef double old_angular_velocity_coefficient = control_args["old angular velocity coefficient"]
    control_value = control_args["old total"]

    new_angular_velocity = state[num_nodes:2*num_nodes]

    # approximate the integral using the trapezoidal rule
    integral_term = (((old_angular_velocity * old_angular_velocity_coefficient)
                      + (new_angular_velocity * (1.0 - old_angular_velocity_coefficient))) * (t - old_time))

    # add to the running_total

    control_value += np.dot(c_mat, integral_term)
    control_value = np.clip(control_value, control_args["control constraints"]["min"],
                            control_args["control constraints"]["max"])
    control_args["old total"] = control_value
    control_args["old angular velocity"] = new_angular_velocity
    control_args["old time"] = t

    return control_value


def gather_and_broadcast(double t, state, control_args):
    c_mat = -control_args["coefficient matrix"]
    cdef int num_nodes = control_args["number of nodes"]
    cdef double old_time = control_args["old time"]
    old_angular_velocity = control_args["old angular velocity"]
    cdef double old_angular_velocity_coefficient = control_args["old angular velocity coefficient"]
    control_value = control_args["old total"]

    new_angular_velocity = state[num_nodes:2*num_nodes]

    # approximate the integral using the trapezoidal rule
    integral_term = (((old_angular_velocity * old_angular_velocity_coefficient)
                      + (new_angular_velocity * (1.0 - old_angular_velocity_coefficient))) * (t - old_time))

    # add to the running_total

    control_value += np.dot(c_mat, integral_term)
    control_value = np.clip(control_value, control_args["control constraints"]["min"],
                            control_args["control constraints"]["max"])
    control_args["old total"] = control_value
    control_args["old angular velocity"] = new_angular_velocity
    control_args["old time"] = t

    return control_value


def linear_rotor_angle(double t, state, control_args):
    cdef int num_nodes = control_args["number of nodes"]
    c_mat = -control_args["coefficient matrix"]

    rotor_angle = state[:num_nodes]
    rotor_angle_init = control_args["rotor angle initial value"]

    control_value = np.dot(c_mat, rotor_angle - rotor_angle_init)
    control_value = np.clip(control_value, control_args["control constraints"]["min"],
                    control_args["control constraints"]["max"])
    return control_value


def piecewise_constant(double t, state, control_args):
    control_parameters = control_args["parameters"]
    cdef int num_partitions = control_args["number of partitions"]
    cdef int num_nodes = control_args["number of nodes"]
    cdef double terminal_time = control_args["terminal time"]
    cdef int knot_index = int(num_partitions * t / terminal_time)
    control_value = control_parameters[knot_index] if knot_index < num_partitions else np.zeros(num_nodes)
    return control_value


def control(double t, state, control_args):
    control_value = control_args["func"](t, state, control_args["args"])
    return control_value