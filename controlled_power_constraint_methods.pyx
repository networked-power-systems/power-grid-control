from common_imports import *
import frequency_methods_enhanced as frequency_methods_e
import frequency_methods
import constraints


########################################################################################################################
"""
Constraint loss function
"""
########################################################################################################################


def controlled_power_lagrange(control_values, int num_nodes, int this_node, controlled_power_constraint):
    cdef double control_value = control_values[this_node]
    cdef double lagrange_cost = constraints.inequality_constraint_helper(control_value, controlled_power_constraint, constraints.box_constraint)
    return lagrange_cost


def controlled_power_cost_from_ts(double s, int this_node, control_args, enhancing_control_args, int num_partitions, int num_nodes,
                                  controlled_power_constraint):
    control_values = frequency_methods_e.control(s, control_args, enhancing_control_args)
    cdef double f_value_time = frequency_methods_e.enhancing_control_equation_of_motion(s, *enhancing_control_args)
    cdef double cost = f_value_time * controlled_power_lagrange(control_values, num_nodes, this_node,
                                                                controlled_power_constraint)
    return cost


def controlled_power_performance(int this_node, fixed_knots, control_args, enhancing_control_args, int num_partitions, int num_nodes,
                                 controlled_power_constraint):
    cdef double s
    cdef double performance_value = simps([controlled_power_cost_from_ts(s, this_node, control_args, enhancing_control_args, num_partitions, num_nodes,
                                           controlled_power_constraint) for s in fixed_knots], x = fixed_knots)
    return performance_value